<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('index');
});
*/
Route::get('/', [App\Http\Controllers\IndexController::class, 'index'])->name('index');

Route::get('/users', function () {
    return view('users');
});

Route::resource('paket','App\Http\Controllers\PaketController');

//Route::get('/paket/{id}', 'App\Http\Controllers\PaketController@show');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
