<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_show_page_user()
    {
        $response = $this->get('/users');

        //$response->assertStatus(200);
        $response->assertViewIs('users');
    }
}
