<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Models\Paket;
use App\Models\AmbilPaket;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Paket::all();
        //$paket = AmbilPaket::where('id_user', Auth::id())->get();

        $paket = DB::table('ambil_pakets')
        ->join('pakets', 'ambil_pakets.idpaket', '=', 'pakets.idpaket')        
        ->select('ambil_pakets.*','pakets.*')
        ->where('ambil_pakets.id_user', Auth::id())
        ->get();

        return view('home', compact('data','paket'));        
    }
}
