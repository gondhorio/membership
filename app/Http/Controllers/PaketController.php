<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paket;
use App\Models\AmbilPaket;

class PaketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = 1;// Kategori::all();
        return view('paket.show', compact('data'));
    }

    public function show($id)
    {
        $paket = Paket::where('idpaket', $id)->get();
        return view('paket.show', compact('paket'));
    }

    public function store(Request $request)
    {
        /*
        $request->validate([
            'namakategori' => 'required',
        ]);
*/
        $ambilpaket = new AmbilPaket;

        $ambilpaket->idpaket = $request->idpaket;
        $ambilpaket->id_user = $request->user_id;
        $ambilpaket->tgljamdaftar = date('YmdHis');
        $ambilpaket->tglawal = date('Ymd');
        $ambilpaket->tglakhir = date('Ymd', strtotime(' + '.abs($request->durasi).' months'));
        $ambilpaket->statuspaket = 1;
        
        $ambilpaket->save();
/*
        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Create Kategori";
        $aktifitas->deskripsi = $request->namakategori;
        $aktifitas->user_id = Auth::id();

        $aktifitas->save();

        Alert::success('Yeaaayy', 'Data kategori berhasil ditambahkan');
*/
    	return redirect('/home');
    }
}
