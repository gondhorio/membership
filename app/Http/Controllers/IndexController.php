<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paket;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest'); 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Paket::all();
        return view('index', compact('data'));     
    }
}
