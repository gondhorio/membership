<!-- Sidebar Start -->
        <div class="sidebar pe-4 pb-3"> 
            <nav class="navbar bg-light navbar-light">
                <a href="#" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary" style="margin:0; padding:0;"><i class="fa fa-hashtag me-2"></i>ASK.COM</h3>
					<span style="font-size:12px; margin:0; padding:0; position:absolute; top:1; padding-left:30px">Forum Tanya Jawab</span>
                </a>
                <div class="d-flex align-items-center ms-4 mb-4" style="margin-top:20px;">
                    <div class="position-relative">
                        @if (!Auth::user()->profile->avatar == null)
                            <img src="{{ asset('images/' . Auth::user()->profile->avatar) }}" alt="thumbnail_profile" class="rounded-circle" width="40px" height="40px">
                        @else
                            <img src="{{ asset('images/default-profile.png') }}" alt="thumbnail_profile" class="rounded-circle" width="40px" height="40px">            
                        @endif
                        <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
                    </div>
                    <div class="ms-3">
                        <h6 class="mb-0">{{Auth::user()->name}}</h6>
                        <span>Admin</span>
                    </div>
                </div>
                <div class="navbar-nav w-100">
                    <a href="/admin" class="nav-item nav-link active"><i class="fa fa-home me-2"></i>Home</a>
                    <hr />
                    <a href="/kategori" class="nav-item nav-link"><i class="fa fa-book me-2"></i>My Category</a>
                    <a href="/tanya/create" class="nav-item nav-link"><i class="fa fa-paper-plane me-2"></i>Post Question</a>
					<hr />
                    <a href="/tanya" class="nav-item nav-link"><i class="fa fa-table me-2"></i>My Q & A</a>
                    <a href="/print-pdf" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Reporting</a>
                   
                </div>
            </nav>
        </div>
        <!-- Sidebar End -->