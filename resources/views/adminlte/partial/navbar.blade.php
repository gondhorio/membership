<!-- Navbar Start -->
<nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0"> 
                <a href="/" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary" style="margin:0; padding:0;"><i class="fa fa-hashtag me-2"></i>Membership.com</h3>
                </a>
                              
                <div class="navbar-nav align-items-center ms-auto">                  
                     <div class="d-flex ms-lg-4">
                            <a href="{{ route('logout') }}" class="btn btn-outline-primary" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>                         
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->