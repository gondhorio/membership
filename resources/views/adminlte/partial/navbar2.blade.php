<!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0"> 
                <a href="/" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary" style="margin:0; padding:0;"><i class="fa fa-hashtag me-2"></i>Membership.com</h3>
                </a>
                              
                <div class="navbar-nav align-items-center ms-auto">                  
                     <div class="d-flex ms-lg-4"><a class="btn btn-outline-primary" href="/login">Sign In</a><a class="btn btn-primary ms-3" href="/register">Sign Up</a></div>
                </div>
            </nav>
            <!-- Navbar End -->