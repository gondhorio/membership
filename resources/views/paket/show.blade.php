@extends('adminlte.master2')

@section('content') 
	
	<div class="card-body">		
		<div class="d-flex w-100 align-items-center justify-content-between">
			<button type="button" class="btn btn-primary" onclick="document.location.href='{{ route('home') }}'"><i class="fa fa-arrow-circle-left"></i> Back</button>
			<a href="/" class="navbar-brand">
                <h3 class="text-primary" style="margin:0; padding:0;"><i class="fa fa-hashtag me-2"></i>Membership.com</h3>
            </a>
		</div>
		<hr/>

		<form action="{{ route('paket.index') }}" method="post">
		@csrf
		<div class="card-body">
			<div class="form-group">
				<input type="hidden" name="user_id" value="{{Auth::id()}}">
				<input type="hidden" name="idpaket" value="{{$paket[0]->idpaket}}">
				<input type="hidden" name="durasi" value="{{$paket[0]->durasi}}">
			</div>

			<h3>Paket Keanggotaan</h3><br>
			<h5>Nama Paket : {{$paket[0]->namapaket}}</h5>
			<h5>Harga : Rp. {{number_format($paket[0]->harga)}}</h5>
			<h5>Durasi : {{$paket[0]->durasi}} Bulan</h5>

			<h5>Tanggal Registrasi : {{date('Y-m-d')}}</h5>
			<h5>Tanggal Kadaluarsa : {{date('Y-m-d', strtotime(' + '.abs($paket[0]->durasi).' months'))}}</h5>
						
			</div>
			
			<div class="card-footer">
				<button type="submit" class="btn btn-warning">Daftar Sekarang</button>
			</div>
		</form>

		
	</div>
	
@endsection

@push('scripts')
	
@endpush