@extends('adminlte.master')

@section('content') 
	
	<div class="card-body">
		<div class="d-flex w-100 align-items-center justify-content-between">
			<a href="{{ route('kategori.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
			<h5>List Data Kategori</h5>
		</div>
		<hr/>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="width: 10px">#</th>
					<th>Nama Kategori</th>					
					<th style="width: 40px">Action</th>
				</tr>
			</thead>
			<tbody>
                    
            </tbody>
		</table>
	</div>
	
@endsection

@push('scripts')
	
@endpush