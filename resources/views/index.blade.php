@extends('adminlte.master2') 

@section('content')
	<!-- Navbar Start -->
	@include('adminlte.partial.navbar2')
	<!-- Navbar End -->
	
	<main class="main" id="top">
     
		<section class="pt-7">
			<div class="container">
			  <div class="row align-items-center">
				<div class="col-md-12 text-md-start text-center py-6">
				  <h1 class="mb-4 fs-9 fw-bold">Membership Web App</h1>
				  <p class="mb-6 lead text-secondary">Merupakan Sebuah Aplikasi Web untuk menjalankan sistem keanggotaan 
				  </p>
				</div>
				<div class="col-md-12" style="padding:50px" aling="center">
					<h2 class="mb-4 fs-9 fw-bold">Pilih Paket Keanggotaan Kamu</h2>
					<div class="row align-items-center">
					@forelse ($data as $key=>$value)
						<div class="col paketbox" >
							<h2 class="mb-4 fs-9 fw-bold">{{$value->namapaket}}</h2>
							<p class="mb-6 lead text-secondary"><b>Durasi :</b> {{$value->durasi}} Bulan</p>
							<p class="mb-6 lead text-secondary"><b>Harga :</b> Rp. {{number_format($value->harga)}}</p>				
							<div class="text-center"><a class="btn btn-warning" href="/home" role="button">Pilih Paket</a></div>											  			
						</div>
					@empty
						<div>Tidak ditemukan data</div>
					@endforelse   
					</div>
				</div>
			  </div>
			</div>
		  </section>
	</main>
		
@endsection

@push('scripts')
	<script>
	  
	</script>
@endpush