<h1> <b>Membership Web App</b> </h1>

<h2> <b>Setup Awal</b> </h2>
<ul>
    <li> Masuk di folder project membership</li>
    <li> Sesuaikan setting koneksi database pada file .env</li>
    <li> Jalankan npm install</li>
    <li> Jalankan "php artisan migrate" (untuk database migrate)</li>
    <li> Setelah itu Jalankan "php artisan db:seed" (untuk database seed nya)</li>
</ul>

<h2> <b>Desain ERD</b> </h2>

<p><img src="./public/images/erd.png" alt="Gambar_ERD"></p>
<p> ini merupakan desain relasi antar tabel, terdapat tabel paket (untuk master), tabel users (bawaan laravel), tabel ambilpaket dan tabel log user</p>

<h2> <b>Link Video Demo</b> </h2>

<a href="https://www.loom.com/share/2fcf65e74f8b4a678f378df95c97a651"> Demo Singkat Aplikasi</a>