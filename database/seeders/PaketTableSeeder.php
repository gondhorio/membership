<?php

namespace Database\Seeders;
use App\Models\Paket;
use Illuminate\Database\Seeder;


class PaketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Paket::create([
            'idpaket'	=> 1,
            'namapaket'	=> 'Basic',
            'durasi'	=> 1, //durasi (dalam bulan)
            'harga'	=> 50000,
            'flagaktif'	=> true
        ]);
        
        Paket::create([
            'idpaket'	=> 2,
            'namapaket'	=> 'Middle',
            'durasi'	=> 3, //durasi (dalam bulan)
            'harga'	=> 130000,
            'flagaktif'	=> true
        ]);
        Paket::create([
            'idpaket'	=> 3,
            'namapaket'	=> 'Advance',
            'durasi'	=> 6, //durasi (dalam bulan)
            'harga'	=> 250000,
            'flagaktif'	=> true
        ]);
        
    }
}
