<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'	=> 1,
            'email'	=> 'gondhorio@gmail.com',
            'name'	=> 'Khomsun',
            'password'	=> bcrypt('secret'),
            'nowa'	=> '08123456789'
        ]);
    }
}
