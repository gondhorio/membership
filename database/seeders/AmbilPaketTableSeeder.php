<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AmbilPaket;

class AmbilPaketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $durasi = 1;

        AmbilPaket::create([
            'idpaket'	=> 2,
            'id_user'	=> 1,
            'tgljamdaftar'	=> date('YmdHis'),
            'tglawal'	=> date('Ymd'),
            'tglakhir'	=> date('Ymd', strtotime(' + '.$durasi.' months')),
            'statuspaket'	=> 1
        ]);
    }
}
