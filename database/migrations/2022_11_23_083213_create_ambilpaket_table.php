<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmbilpaketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambil_pakets', function (Blueprint $table) {
            $table->unsignedBigInteger('idpaket');
            $table->unsignedBigInteger('id_user');
            $table->string('tgljamdaftar', 14);
            $table->string('tglawal', 8);
            $table->string('tglakhir', 8);
            $table->tinyInteger('statuspaket');       
            $table->timestamps();     
            $table->foreign('idpaket')->references('idpaket')->on('pakets');
            $table->foreign('id_user')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ambil_pakets');
    }
}
